#!/bin/bash

set -x
java -version
mvn -v
mvn -B clean

#SimpleAPI
if [ -f fetch_dependencies.sh ]; then
    bash fetch_dependencies.sh
fi

wget https://gitlab.com/cubekrowd/mavenrepo/raw/master/settings.xml
mvn -B deploy -s settings.xml
