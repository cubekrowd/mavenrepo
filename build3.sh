#!/bin/bash

set -x
java -version
mvn -v
mvn -B clean
mvn -B versions:set -DnewVersion=$1.$CI_PIPELINE_ID
wget https://gitlab.com/cubekrowd/mavenrepo/raw/master/settings.xml
mvn -B package -s settings.xml
rm target/original-*.jar
cp target/*.jar ./
