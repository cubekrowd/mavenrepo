#!/bin/bash

set -x

export MAVEN_OPTS="-Dmaven.repo.local=.m2/repository -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true"
export MAVEN_CLI_OPTS="--batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true"

java -version
mvn -v
mvn -B clean

wget https://gitlab.com/cubekrowd/mavenrepo/raw/master/settings.xml
mvn -B lombok:delombok javadoc:aggregate -s settings.xml
mv target/site/apidocs public
