#!/bin/bash

set -x
java -version
mvn -v
mvn -B clean

#SimpleAPI
if [ -f fetch_dependencies.sh ]; then
    bash fetch_dependencies.sh
fi

wget https://gitlab.com/cubekrowd/mavenrepo/raw/master/settings.xml
mvn -B package -s settings.xml

#SimpleAPI
if [ -f fetch_dependencies.sh ]; then
  cp API/target/SimpleAPI-IGNORE.jar ./SimpleAPI.jar
else
  rm target/original-*.jar
  cp target/*.jar ./
fi
